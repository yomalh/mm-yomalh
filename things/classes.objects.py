class Vehicles:
    name = " "
    price = " "
    kind = " "

    def description(self):
        desc_str = "%s is a %s worth $%.2f." % (self.name, self.kind, self.price)
        return desc_str


car1 = Vehicles()
car2 = Vehicles()

car1.name = "Fer"
car1.price = 60000.00
car1.kind = "convertible"

car2.name = "jump"
car2.price = 10000.00
car2.kind = "van"

print(car2.description())
print(car1.description())
