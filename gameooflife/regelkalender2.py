# Kästchen eintragen
def updateGitter(y, x, value, Gittertowrite):
    Gittertowrite[y].pop(x)
    Gittertowrite[y].insert(x, [value])
    return Gittertowrite


# Die Summe aller Nachbarkästchen (8stk)
def sumneighbours(hoehe, breite, Gitter):
    neighbours = []
    for hoehe in range(hoehe - 1, hoehe + 2):
        if hoehe > len(Gitter) - 2:
            hoehe -= 1
        for breite in range(breite - 1, breite + 2):
            if breite > len(Gitter) - 2:
                breite -= 1
            neighbours.append(Gitter[hoehe][breite][0])
    return sum(neighbours)


def checkrules(y, x, Gittertocheck, Gittertowrite):
    livingneighbours = sumneighbours(y, x, Gittertocheck)
    if Gittertocheck[y][x] == 0 and livingneighbours == 3:
        return updateGitter(x, y, 1, Gittertowrite)
    elif Gittertocheck[y][x] == 0:
        return Gittertowrite
    else:
        if livingneighbours < 2:
            return updateGitter(y, x, 0, Gittertowrite)
        elif livingneighbours < 4:
            return Gittertowrite
        else:
            return updateGitter(y, x, 0, Gittertowrite)

def schritt(vorherigesGitter):
    Gittertowrite = vorherigesGitter
    h = 0
    for hoehe in Gitter:

        b = 0
        for breite in hoehe:
            # Gittertowrite wird HOFFENTLICH immer ein neuer wert zugewiesen,der in der nöchsten runde wieder checkrules übergeben wird
            Gittertowrite = checkrules(h, b, vorherigesGitter, Gittertowrite)
            b += 1
        h += 1
    return Gittertowrite



# Das Raster mit einer Weite von Weite
Weite = 10
Gitter = []
for i in range(0, Weite):
    Zeileunw = []
    for j in range(0, Weite):
        Zeileunw.append([0])
    Gitter.append(Zeileunw)
