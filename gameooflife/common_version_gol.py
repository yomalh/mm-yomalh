class Gameoflive:
    def __init__ (self,xachse = 6,yachse = 6):
        self.xachse = xachse
        self.yachse = yachse
        nummerierung= 0
        self.spielfeld= []
        for x in range(xachse):
            """ spielfeld bauen, nummerieren
            """
            self.spielfeld.append([])
            for y in range(yachse):
                self.spielfeld[x].append(nummerierung)
                nummerierung += 1

    def matrixprint(self):
        """ Die Funktion gibt das Spielfeld aus.
            beachtet y * x und füllt auf zwei Ziffern auf
            s = spielfeld
        """
        for i in range(len(self.spielfeld)):
            for j in range(len(self.spielfeld[i])):
                print(f"%02d" % (self.spielfeld[i][j]), end =" ")
            print("")

    def checkstatus(self,x, y):
        """ Nachbarn herausfinden
        """
        lebende = 0
        if self.spielfeld[x-1][y-1] >=1:
            lebende += +1
        if self.spielfeld[x][y-1] >=1:
            lebende += +1
        if self.spielfeld[x+1][y-1] >=1:
            lebende += +1
        if self.spielfeld[x+1][y] >=1:
            lebende += +1
        if self.spielfeld[x+1][y+1] >=1:
            lebende += +1
        if self.spielfeld[x][y+1] >=1:
            lebende += +1
        if self.spielfeld[x-1][y+1] >=1:
            lebende += +1
        if self.spielfeld[x-1][y] >=1:
            lebende += +1
        return  lebende

gameoflive = Gameoflive()
gameoflive.matrixprint()
print(gameoflive.checkstatus(1,1))
print(gameoflive.checkstatus(0,0))