def updateGitter(y, x, value, Gittertowrite):
    Gittertowrite[y].pop(x)
    Gittertowrite[y].insert(x, [value])
    return Gittertowrite


def sumneighbours(hoehe, breite, Gitter):
    neighbours = []
    for hohe in range(hoehe - 1, hoehe + 2):
        if hohe > len(Gitter) - 1:
            hohe -= 1
        for breide in range(breite - 1, breite + 2):
            if breide > len(Gitter) - 1:
                breide -= 1
            if hoehe == hohe and breite == breide and Gitter[hoehe][breite] != 0:
                pass
            else:
                neighbours.append(Gitter[hohe][breide][0])
    return sum(neighbours)


def checkrules(y, x, Gittertocheck):
    Gittertowrite = Gittertocheck
    Kaestchen = Gittertocheck[y][x][0]
    livingneighbours = sumneighbours(y, x, Gittertocheck)
    if Kaestchen == 0 and livingneighbours == 3:
        return updateGitter(y, x, 1, Gittertowrite)
    elif Kaestchen == 0:
        return Gittertowrite
    else:
        if livingneighbours < 2:
            return updateGitter(y, x, 0, Gittertowrite)
        elif livingneighbours < 4:
            return Gittertowrite
        else:
            return updateGitter(y, x, 0, Gittertowrite)


Gitter = [[[0], [0], [], [0], [0]], [[0], [0], [0], [0], [0]], [[0], [1], [1], [1], [0]], [[0], [0], [0], [0], [0]],
          [[0], [0], [0], [0], [0]]]

for liste in checkrules(1, 3,Gitter):
    print(liste, "\n")
print(
    "___________________________________________________________________________________________________\nnächstes Gitter")
