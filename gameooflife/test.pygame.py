# Importieren der Pygame-Bibliothek
import pygame
Weite = 5
Gitter = []
for i in range(0, Weite):
    Zeileunw = []
    for j in range(0, Weite):
        Zeileunw.append([0])
    Gitter.append(Zeileunw)
def updateGitter(y, x, value, Gittertowrite):
    Gittertowrite[y].pop(x)
    Gittertowrite[y].insert(x, [value])
    return Gittertowrite

updateGitter(1, 1, 1, Gitter)
updateGitter(1, 2, 1, Gitter)
updateGitter(1, 3, 1, Gitter)

# initialisieren von pygame
pygame.init()
clock = pygame.time.Clock()
# genutzte Farbe
SCHWARZ = ( 0, 0, 0)
WEISS   = ( 255, 255, 255)
ORANGE = (255, 140, 0)

# Fenster öffnen
fenster = pygame.display.set_mode((1000, 1000))
#Titel
pygame.display.set_caption("game of life")
screen = pygame.display.set_mode((1000, 1000))

# solange die Variable True ist, soll das Spiel laufen
spielaktiv = True

# Schleife Hauptprogramm
while spielaktiv:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            spielaktiv = False
            print("Spieler hat Quit-Button angeklickt")

    #Spiellogik hier integrieren
    # Kästchen eintragen


    # Die Summe aller Nachbarkästchen (8stk)
    def sumneighbours(hoehe, breite, Gitter):
        neighbours = []
        for höhe in range(hoehe - 1, hoehe + 2):
            if höhe > len(Gitter) - 2:
                höhe -= 1
            for breide in range(breite - 1, breite + 2):
                if breide > len(Gitter) - 2:
                    breide -= 1
                if hoehe == höhe and breite == breide and Gitter[hoehe][breite] != 0:
                    pass
                else:
                    neighbours.append(Gitter[höhe][breide][0])
        return sum(neighbours)


    # eingabe der Kästchen in das Gitter, beendet abfrage selbstständig

    def eingabe(Weite, Gitter):
        weitere_Eingabe = input("Ein Kästchen hinzufügen: \nY/N\n")
        if weitere_Eingabe == "Y":
            y, x = getKästchen(Weite)
            updateGitter(y, x, 1, Gitter)
            eingabe(Weite, Gitter)
        return


    # Durchläuft die Regeln 1-4:
    """    Any live cell with fewer than two live neighbours dies, as if by underpopulation.
        Any live cell with two or three live neighbours lives on to the next generation.
        Any live cell with more than three live neighbours dies, as if by overpopulation.
        Any dead cell with exactly three live neighbours becomes a live cell, as if by reproduction."""


    def checkrules(y, x, Gittertocheck, Gittertowrite):
        Kaestchen = Gittertocheck[y][x][0]
        livingneighbours = sumneighbours(y, x, Gittertocheck)
        if Kaestchen == 0 and livingneighbours == 3:
            return updateGitter(x, y, 1, Gittertowrite)
        elif Kaestchen == 0:
            return Gittertowrite
        else:
            if livingneighbours < 2:
                return updateGitter(y, x, 0, Gittertowrite)
            elif livingneighbours < 4:
                return Gittertowrite
            else:
                return updateGitter(y, x, 0, Gittertowrite)


    # _______________________________________________________________________________________________________#

    # Das Raster mit einer Weite von Weite
    Weite = 5
    Gitter = []
    for i in range(0, Weite):
        Zeileunw = []
        for j in range(0, Weite):
            Zeileunw.append([0])
        Gitter.append(Zeileunw)


    # --------------Verlauf des Spiels---------------
    # erstens Eingabe
    # eingabe(Weite, Gitter)

    # zweitens -----------Weiterentwickeln-------------8
    # Gittertowrite ist das nöchste Gitter, was aber während des Durchgehens aller pixel noch nicht genutzt werden kann, da es sich verändert

    def schritt(vorherigesGitter):
        Gittertowrite = vorherigesGitter
        h = 0
        for hoehe in Gitter:

            b = 0
            for breite in hoehe:
                # Gittertowrite wird HOFFENTLICH immer ein neuer wert zugewiesen,der in der nöchsten runde wieder checkrules übergeben wird
                Gittertowrite = checkrules(h, b, vorherigesGitter, Gittertowrite)
                b += 1
            h += 1
        return Gittertowrite


    Gitter = schritt(Gitter)
    #Spielfeld löschen
    screen.fill(WEISS)

    #Spielfeld/figuren zeichnen
    def kor(zahl):
        zahl = zahl * Weite
        return zahl


    def element_zeichnen(reihe, spalte):
        pygame.draw.rect(fenster, ORANGE, (spalte * Weite, reihe * Weite, Weite, Weite))


    for x in range(0, Weite):
        for y in range(0, Weite):
            if Gitter[y][x] != 0:
                element_zeichnen(x, y)
    # Fenster aktualisieren
    #Gittertowrite ????
    #Fenster aktualisieren
    pygame.display.flip()

#refresh- zeiten festlegen
    clock.tick(60)

pygame.quit()
