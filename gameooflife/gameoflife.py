# Kästchen abfragen für Anfangsmuster
def getKästchen(Weite):
    print("Gib die Zeile(1-", Weite, ") und dann die Spalte(1-", Weite, ") für ein Anfangskästchen ein:\n")
    Kästchen_y = int(input("Zeile(1-", Weite, "):"))
    Kästchen_x = int(input("Spalte(1-", Weite, "):"))
    return Kästchen_y - 1, Kästchen_x - 1


# eingabe der Kästchen in das Gitter, beendet abfrage selbstständig

def eingabe(Weite, Gitter):
    weitere_Eingabe = input("Ein Kästchen hinzufügen: \nY/N\n")
    if weitere_Eingabe == "Y":
        y, x = getKästchen(Weite)
        updateGitter(y, x, 1, Gitter)
        eingabe(Weite, Gitter)
    return


# Kästchen eintragen
def updateGitter(y, x, value, Gittertowrite):
    Gittertowrite[y].pop(x)
    Gittertowrite[y].insert(x, [value])
    return Gittertowrite


# Die Summe aller Nachbarkästchen (8stk)
def sumneighbours(hoehe, breite, Gitter):
    neighbours = []
    for hohe in range(hoehe - 1, hoehe + 2):
        if hohe > len(Gitter) - 1:
            hohe -= 1
        for breide in range(breite - 1, breite + 2):
            if breide > len(Gitter) - 1:
                breide -= 1
            if hoehe == hohe and breite == breide and Gitter[hoehe][breite] != 0:
                pass
            else:
                neighbours.append(Gitter[hohe][breide][0])
    return sum(neighbours)


# Durchläuft die Regeln 1-4:
"""    Any live cell with fewer than two live neighbours dies, as if by underpopulation.
    Any live cell with two or three live neighbours lives on to the next generation.
    Any live cell with more than three live neighbours dies, as if by overpopulation.
    Any dead cell with exactly three live neighbours becomes a live cell, as if by reproduction."""


def checkrules(y, x, Gittertocheck):
    Gittertowrite = Gittertocheck
    Kaestchen = Gittertocheck[y][x][0]
    livingneighbours = sumneighbours(y, x, Gittertocheck)
    if Kaestchen == 0 and livingneighbours == 3:
        return updateGitter(y, x, 1, Gittertowrite)
    elif Kaestchen == 0:
        return Gittertowrite
    else:
        if livingneighbours < 2:
            return updateGitter(y, x, 0, Gittertowrite)
        elif livingneighbours < 4:
            return Gittertowrite
        else:
            return updateGitter(y, x, 0, Gittertowrite)


# _______________________________________________________________________________________________________#


# Das Raster mit einer Weite von Weite
Weite = 5
Gitter = []
for i in range(0, Weite):
    Zeileunw = []
    for j in range(0, Weite):
        Zeileunw.append([0])
    Gitter.append(Zeileunw)


# --------------Verlauf des Spiels---------------
# erstens Eingabe
# eingabe(Weite, Gitter)


# zweitens -----------Weiterentwickeln-------------8
# Gittertowrite ist das nöchste Gitter, was aber während des Durchgehens aller pixel noch nicht genutzt werden kann, da es sich verändert

def schritt(vorherigesGitter):
    Gittertowrite = vorherigesGitter
    h = 0
    for hoehe in Gitter:

        b = 0
        for breite in hoehe:
            # Gittertowrite wird HOFFENTLICH immer ein neuer wert zugewiesen,der in der nöchsten runde wieder checkrules übergeben wird
            Gittertowrite = checkrules(h, b, vorherigesGitter)
            b += 1
        h += 1
    return Gittertowrite


updateGitter(2, 1, 1, Gitter)
updateGitter(2, 2, 1, Gitter)
updateGitter(2, 3, 1, Gitter)

Rasta = Gitter
while True:
    # print(Rasta)
    for liste in Rasta:
        print(liste, "\n")
    print(
        "___________________________________________________________________________________________________\nnächstes Gitter")
    Rasta = schritt(Rasta)
