

import sqlite3
verbindung = sqlite3.connect("geburtstage.db")
zeiger = verbindung.cursor()

nachname   = "Schiller"
vorname    = "Friedrich"
geburtstag = "10.11.1759"

zeiger.execute("""
                INSERT INTO personen 
                       VALUES (?,?,?)
               """,
              (vorname, nachname, geburtstag)
              )

verbindung.commit()
verbindung.close()
