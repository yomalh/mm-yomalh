# euler24


weite = 4
# list means the first permutation / hopefully in lexicographic order
list = []
for i in range(0, weite):
    list.append(i)


# list = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
# print(nextpermutation(list))


# before refers to the perumtation from before
def findupper(before):
    for index in range(0, weite - 1):
        # print("findupper, index:", index)
        # number most on the right from which the next number is bigger
        if before[index] < before[index + 1]:
            uppersindex = index
    # print("uppersindex: ", uppersindex)
    return uppersindex


def findnext(before, uppersindex):
    # find number higher than upper
    # preferably upper + 1
    ceiling = weite
    for index in range(uppersindex + 1, weite):
        if before[index] > before[uppersindex]:
            eventualceiling = before[index]
            if ceiling > eventualceiling:
                ceilingsindex = index
    # print("ceilingsindex: ",ceilingsindex)
    return ceilingsindex


def nextpermutation(before):
    uppersindex = findupper(before)
    ceilingsindex = findnext(before, uppersindex)

    # swaps upper and ceiling

    upper = before[uppersindex]
    ceiling = before[ceilingsindex]

    before.pop(ceilingsindex)
    before.insert(ceilingsindex, upper)
    before.pop(uppersindex)
    before.insert(uppersindex, ceiling)
    # if the number behind them (including ceiling) is bigger
    # than its reverse, reverse it
    if before[weite - 1] < before[ceilingsindex]:
        # reverse from ceiling
        for digit in range(ceilingsindex, weite - 1):
            placeit = before.index(digit)
            last = before.pop(weite - 1)
            before.insert(placeit, last)

    return before


count = 0
while count < 1000000:
    list = nextpermutation(list)
    # print(list)
    count += 1
    print(list)
    if count == 60:
        print(list)
