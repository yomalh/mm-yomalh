#euler3
import logging
import math
logging.basicConfig(level=logging.DEBUG)


#getting factor of number
def getfactors(num):
    factors = []
    for potfac in range(1, int(math.sqrt(num)) +1):
        if num % potfac == 0:
            factors.append(potfac)
            factors.append(num // potfac)
    return factors



#determine if a number is prime
def isprime(num):
    return len(getfactors(num)) == 2

#logging.debug('isprime(24) = %s' % (isprime(24)))
#logging.debug('isprime(24) = %s' % (isprime(17)))

#allfactors = getfactors(600851475143)
allfactors = getfactors(4030457096782241)
#find highest num
largestprimefactor =0
for factor in allfactors:
    if isprime(factor) and factor > largestprimefactor:
        largestprimefactor = factor
print(largestprimefactor)

