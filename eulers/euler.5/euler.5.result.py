import logging

# diese Funktion prüft auf Teilbarkeit von 11 bis 20
def check(n):
    # Liste der Teiler
    d = list(range(11,20))
    for i in d:
        if n % i != 0:
            return False
    return True

def solve():
    step = 200000
    # die aktuell zu prüfende Zahl
    toCheck = step
    result = 0
    while(not check(toCheck)):
        toCheck = toCheck + step
        result = toCheck
    logging.info(result)
