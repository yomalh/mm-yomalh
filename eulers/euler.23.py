# euler23
# perfect numbers
# abundant numbers

# sum of proper divisors higher than number
# an abundant = abun = a bun
# many abundants/buns are buns
import math


def findabundents(x):
    divsum = 1
    for psbldiv in range(2, math.floor(math.sqrt(x)) + 1):
        if x % psbldiv == 0:
            div2 = x / psbldiv
            if not div2 == psbldiv:
                divsum += div2 + psbldiv

        if divsum > x:
            return True
    return False
    # print(x, "<=" ,divsum)
    # prints out nonabundants

setnaturals = set(range(1,28123+1,1))

buns = []
for i in range(12, 28123+1):
    ifabundant = findabundents(i)
    # print(abun)
    if ifabundant:
        buns.append(i)
# print(buns)
listofsumsofbuns = []
#print("BUNS:\n", buns)
for e1 in buns:
    for e2 in buns:
        sum = e1 + e2

        listofsumsofbuns.append(sum)
listofsumsofbuns.sort()

setofsumsofbuns = set(listofsumsofbuns)
print("SUMSOFBUNS:\n",setofsumsofbuns)

result = setnaturals-setofsumsofbuns
print(result)
endresult = 0
for i in result:
    endresult += i
    print(endresult, "+", i)
