import urllib.request

url = 'https://s3-api.us-geo.objectstorage.softlayer.net/cf-courses-data/CognitiveClass/PY0101EN/labs/example1.txt'
filename = 'Example1.txt'
urllib.request.urlretrieve(url, filename)

Lines = [" This is line A\n", "This is line B\n"]
with open("Example1.txt", "w") as File1:
    for line in File1:
        File1.write("This is line ", line, " !\n")^