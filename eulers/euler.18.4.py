#from termcolor import colored
import math
import random


def getTree(static, rows=14):
    if static:
        return [
            [75],
            [95, 64],
            [17, 47, 82],
            [18, 35, 87, 10],
            [20, 4, 82, 47, 65],
            [19, 1, 23, 75, 3, 34],
            [88, 2, 77, 73, 7, 63, 67],
            [99, 65, 4, 28, 6, 16, 70, 92],
            [41, 41, 26, 56, 83, 40, 80, 70, 33],
            [41, 48, 72, 33, 47, 32, 37, 16, 94, 29],
            [53, 71, 44, 65, 25, 43, 91, 52, 97, 51, 14],
            [70, 11, 33, 28, 77, 73, 17, 78, 39, 68, 17, 57],
            [91, 71, 52, 38, 17, 14, 91, 43, 58, 50, 27, 29, 48],
            [63, 66, 4, 68, 89, 53, 67, 30, 73, 16, 69, 87, 40, 31],
            [4, 62, 98, 27, 23, 9, 70, 98, 73, 93, 38, 53, 60, 4, 23]
        ]
    else:
        tree = []
        col = 1
        for i in range(0, rows):
            row = []
            for j in range(0, col):
                row.append(random.randint(1, 99))
            col += 1
            tree.append(row)
        return tree


class TreeNode:
    def __init__(self, l, o, v):
        self.__l = l
        self.__o = o
        self.__v = v

    def level(self):
        return self.__l

    def offset(self):
        return self.__o

    def value(self):
        return self.__v

    def __str__(self):
        return f"treeNode: value=%02d / level=%01d / offset=%01d" % (self.value(), self.level(), self.offset())

    def __repr__(self):
        return f" %02d <%01d/%01d> " % (self.value(), self.level(), self.offset())


def pickNode(t, level=0, offset=0):
    if level < 0 or level >= len(t):
        return None
    if offset < 0 or offset >= len(t[level]):
        return None

    return TreeNode(level, offset, t[level][offset])


def pickSiblingLeft(t, node):
    level = node.level()
    offset = node.offset()

    if level < 0 or level + 1 >= len(t):
        return None
    if offset < 0 or offset >= len(t[level + 1]):
        return None

    return TreeNode(level + 1, offset, t[level + 1][offset])


def pickSiblingRight(t, node):
    level = node.level()
    offset = node.offset()

    if level < 0 or level + 1 >= len(t):
        return None
    if offset + 1 < 0 or offset + 1 >= len(t[level + 1]):
        return None

    return TreeNode(level + 1, offset + 1, t[level + 1][offset + 1])


def pickParents(t, node):
    p = pickParentLeft(t, node)
    p.extend(pickParentRight(t, node))
    return p


def pickParentLeft(t, node):
    level = node.level()
    offset = node.offset()

    if level - 1 < 0 or level >= len(t):
        return []
    if offset - 1 < 0 or offset > len(t[level - 1]):
        return []

    return [TreeNode(level - 1, offset - 1, t[level - 1][offset - 1])]


def pickParentRight(t, node):
    level = node.level()
    offset = node.offset()

    # check if there is level obove
    if level - 1 < 0 or level >= len(t):
        return []
    if offset < 0 or offset >= len(t[level - 1]):
        return []

    return [TreeNode(level - 1, offset, t[level - 1][offset])]


def walk(tree, node):
    if not node:
        return []

    n1 = pickSiblingLeft(tree, node)
    p1 = walk(tree, n1)

    n2 = pickSiblingRight(tree, node)
    p2 = walk(tree, n2)

    p1.extend(p2)
    if (len(p1) == 0):
        p1 = [[node]]
    else:
        for p in p1:
            p.append(node)

    return p1


def bruteForce(aTree):
    p = walk(aTree, pickNode(aTree))
    max = 0
    maxPath = None
    for t in p:
        t.reverse()
        if len(t) != len(t):
            print(">>ERROR1<<", t)
        for i in range(0, len(aTree)):
            n = t[i]
            value = n.value()
            if not value in aTree[i]:
                print(">>ERROR2<<", p[i])

        curMax = 0
        for j in t:
            curMax += j.value()
        if curMax > max:
            max = curMax
            maxPath = t

    printTree(aTree, maxPath, 80)
    print(f"max sum: %d" % max)
    print(f"path count: %d" % len(p))


def printTree(aTree, solution, maxWidth=100):
    offsets = []
    for i in solution:
        offsets.append(i.offset())

    for l in range(0, len(aTree)):
        rowLine = ''
        cRowLine = ''
        for o in range(0, len(aTree[l])):
            color = 'red' if offsets[l] == o else 'blue'
            cRowLine += colored(f"%02d " % aTree[l][o], color)
            rowLine += f"%02s " % aTree[l][o]
        spaces = ' ' * math.floor((maxWidth - len(rowLine.strip())) / 2)
        print(f"%s%s " % (spaces, cRowLine.strip()))


def walkUp(aTree, level, path):
    if level < 0:
        return path

    for p in paths:
        cn = TreeNode(level, paths, aTree[level][i])
        left = pickSiblingLeft(aTree, cn)
        right = pickSiblingLeft(aTree, cn)
        sumLeft = cn.value() + left.value()
        sumRight = cn.value() + right.value()
        if (sumLeft>sumRight):
            p.append(left)
        else:
            p.append(right)
    return walkUp(aTree, level-1, paths)


def moreEfficent(aTree):
    n1 = TreeNode(1, 0, aTree[1][0])
    n2 = TreeNode(1, 1, aTree[1][1])

    p1 = pickParents(aTree, n1)
    print("n1 : ", p1)
    p2 = pickParents(aTree, n2)
    print("n2 : ", p2)
    startLevel = len(aTree)-1
    paths = []
    for i in range(0, startLevel):
        paths.append([])
    walkUp(aTree, startLevel, paths)

    # n1 = TreeNode(1, 0, aTree[1][0])
    # n2 = TreeNode(1, 1, aTree[1][1])
    #
    # p1 = pickParents(aTree, n1)
    # print("n1 : ", p1)
    # p2 = pickParents(aTree, n2)
    # print("n2 : ", p2)


try:
    bruteForce(getTree(True))
    bruteForce(getTree(False, 10))
    # moreEfficent(tree)
    # bruteForce(getTree(True))
    # bruteForce(getTree(False, 10))
    moreEfficent(getTree(True))

except Exception as e:
    print(e)