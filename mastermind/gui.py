import pygame, sys
from pygame.locals import *
from mastermind import Mastermind

import pygame_widgets
from pygame_widgets.button import Button

mm = Mastermind()

mm.start(losung=[1,2,3,4])

# mm.checkcolour()

pygame.init()
fpsClock = pygame.time.Clock()

(boardsize) = (320, 500)
window = pygame.display.set_mode(boardsize)
blue = (0, 0, 255) #1
red = (255, 0, 0) #2
green = (0, 255, 0) #3
white = (205, 205, 205) #4
salmon = (250, 128, 114)
purple = (160, 32, 240)
yellow = (254, 254, 0)

colors= [blue, red, green, white, salmon, purple, yellow]

def initRows(rc, r):
    dbs = [white, white, white, white]
    tbs = [white, white, white, white]

    r[str(rc)] = (dbs, tbs)

def drawit(rc, r):

    yr = 10 + int(rc) * 30
    (dbs, tbs) = r

    # display balls - show result of trying
    pygame.draw.rect(window, dbs[0], (10, yr + 10, 10, 10))
    pygame.draw.rect(window, dbs[1], (20, yr + 10, 10, 10))
    pygame.draw.rect(window, dbs[2], (10, yr + 20, 10, 10))
    pygame.draw.rect(window, dbs[3], (20, yr + 20, 10, 10))

    # test balls - show the user guess
    pygame.draw.circle(window, tbs[0], (50, yr + 20), 10)
    pygame.draw.circle(window, tbs[1], (80, yr + 20), 10)
    pygame.draw.circle(window, tbs[2], (110, yr + 20), 10)
    pygame.draw.circle(window, tbs[3], (140, yr + 20), 10)


rows = {}
for i in range(0,11):
    initRows(i, rows)

def colorButtonClicked(*kwargs):
    global clickedTracker, rows, colors
    print("clicked button with id: %d" % kwargs[0])
    x = clickedTracker % 4
    y = int(clickedTracker / 4)
    (dbs, tbs) = rows[str(len(rows)-1-y)]
    tbs[x] = colors[kwargs[0]]
    clickedTracker += 1

for i in range(0,7):
    button = Button(
        # Mandatory Parameters
        window,  # Surface to place button on
        (i*25),  # X-coordinate of top left corner
        350,  # Y-coordinate of top left corner
        20,  # Width
        20,  # Height

        # Optional Parameters
        text='',  # Text to display
        fontSize=50,  # Size of font
        margin=20,  # Minimum distance between text/image and edge of button
        inactiveColour=colors[i],  # Colour of button when not being interacted with
        hoverColour=colors[i],  # Colour of button when being hovered over
        pressedColour=(0, 200, 20),  # Colour of button when being clicked
        radius=20,  # Radius of border corners (leave empty for not curved)
        onClick=colorButtonClicked,  # Function to call when clicked on
        onClickParams = [i]
    )


clickedTracker = 0

while True:

    for r in rows.keys():
        drawit(r, rows[r])

    for event in pygame.event.get():
        if event.type == QUIT:
            pygame.quit()
            sys.exit()

    pygame_widgets.update(pygame.event.get())
    pygame.display.update()

    fpsClock.tick(30)
