class Mastermind():

    def __init__(self):
        self.losung = []

    def start(self, losung=None):
        if losung == None:
            self.losung = self.mustererstellen()
        else:
            self.losung = losung

    def mustererstellen(self):
        """mustererstellen fragt den Nutzer nach einem Muster aus 6 verschiedene Farben
        gibt dieses in einer Tabelle aus
        es braucht ein try dafür, dass immer nur Zahlen/Farben von 1-6 eingetragen werden
        """
        muster = []
        for i in range(1, 5):
            string = f"gib mir die %d. Zahl von 1-7: " % (i)
            while True:
                try:
                    farbe = int(input(string))
                    break
                except:
                    print("!!!pls enter a number!!!")
            muster.append(farbe)
        return muster

    def checkhits(self, versuch):
        """checkhits findet heraus wieviee Stellen des eingegeben Musters mit der Lösung übereinstimmen
        hitcount zählt diese damit eine Zahl, die in Position und Farbe richtig ist nicht noch bei der Zählung
        zu richtigen Farbe einen Punkt bekommt wird sie hier schon aus dem Versuch gestrichen
        (auffalend, dass man das auch anstatt in funktionen in If-Else stecken kann)
        """
        kopieversuch = versuch.copy()
        kopielosung = self.losung.copy()
        hitcount = 0
        for index in range(0, 4):
            if kopielosung[index] == kopieversuch[index]:
                hitcount += 1
                kopielosung[index] = "schon gezählt"
                kopieversuch[index] = "schon gezaehlt"
        return hitcount, kopielosung, kopieversuch

    def checkcolour(self, losung, versuch):
        """ losung[2,2,2,2], versuch[1,2,3,2]
        gibt die Anzahl dafür, dass eine Farbe aus dem versuch, die nicht schon in Hits zählte, in der Lösung vorkommt
        """
        colourcount = 0
        for index in range(0, 4):
            if self.losung.count(versuch[index]) > 0:
                colourcount += losung.count(versuch[index])
        return colourcount


if __name__ == "__main__":
    ### losung ist die erste eingabe vom Codeerzeuger/Spieler1
    mm = Mastermind()
    mm.start()
    ### versuche werden durch den Codecracker/Spieler2 erstellt
    ersterversuch = mm.mustererstellen()
    hits, losungbearbeitet, versuchbearbeitet = mm.checkhits(ersterversuch)
    print("hits: ", hits)

    print("matching colours: ", mm.checkcolour(losungbearbeitet, versuchbearbeitet))

