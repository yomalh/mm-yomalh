# give statistic of specific digits given in text
file = open("fileerstellen.py", 'r+')

german = "abcdefghijklmnopqrstuvwxyzäüöß 1234567890"
english = "abcdefghijklmnopqrstuvwxyz"
alphabet = german
def readfile(pfad):

    txt = ""
    for Zeile in open(pfad,'r'):
        txt += Zeile
    return txt
text = readfile("./fileerstellen.py")

häufigkeit = {}


for letter in alphabet:
    maleintext = text.count(letter)
    # print(letter, "kommt", maleintext, "mal vor")
    häufigkeit[letter] = maleintext

# print((häufigkeit))
print("length string:", len(text))
summederprozente = 0.0
for i in alphabet:
    prozent = häufigkeit[i] / len(text) * 100
    print("prozentuale Häufigkeit für ", i, "beträgt", prozent, "%")
    summederprozente+= prozent
print(summederprozente)
file.close()
