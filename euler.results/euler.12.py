# euler.12

import math



def checkdivisors(x, minteiler):
    count = 0
    for i in range(1, int(math.sqrt(x) + 1)):
        if x % i == 0:
            count += 2

        if count > minteiler:
            return True, count

    return False, count


# mindestanzahl an teilern
try:
    minteiler = int(input("Gib eine natürliche Zahl:\n"))
except:
    print("geh nach hause, du bist nicht sehr intelligent")
    exit(1)

number = 1

round = 1
while True:
    round += 1
    number += round
    status, count = checkdivisors(number, minteiler)
    if status:
        print("We found ", number, "as result.")
        break
