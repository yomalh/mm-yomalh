# euler15
# different routes in a 20x20 grid
#Baumdiagramm
# routes = []
# list=[]
# rechts = 0
# unten = 1
# # while list not in routes:
#
#     for i in range(0,20):
#         #while sum(list) <21:
#         list.append(rechts)
#         for j in range(0,20):
#
#
# routes.append(list)
#
#
# print(routes)

kantenlänge = 20
lösungen = []

for i in range(1, 2 ** (2 * kantenlänge)):
    b = bin(i)
    bStr = str(b)
    bStr = f"%040d" % (int(bStr[2:]))

    if (bStr.count('1') == kantenlänge):
        lösungen.append(bStr)

print(lösungen)
print(len(lösungen))