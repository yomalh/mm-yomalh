#euler.9
#not mine
import logging
import math


def find(n):
    for a in range(1, n):
        for b in range(1, n):
            c = math .sqrt(a ** 2 + b ** 2)
            if ((c + a + b) == n):
                return (a, b, c)


def solve(n):
    FORMAT = '%(asctime)-20s [%(levelname)s]: %(message)s'
    logging.basicConfig(level=logging.INFO, format=FORMAT)
    (a, b, c) = find(n)
    logging.info(f"%d^2 + %d^2 = %d^2 " % (a, b, c))
    logging.info(f"%d + %d + %d = %d " % (a, b, c, a + b + c))
    #logging.info(f"%d * %d * %d = %d " % (a, b, c, a * b * c))


if __name__ == "__main__":
    solve(1000)
