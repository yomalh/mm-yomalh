longestValue = 1
longestcollartzvalue = 1

limit = 1000000
for startValue in range(2,limit):

    numberofterms =1
    currentterm = startValue

    while currentterm != 1:
        if currentterm % 2 == 0:
            currentterm = currentterm / 2
        elif currentterm % 2 != 0:
            currentterm = (3 * currentterm) + 1
        numberofterms += 1
    if numberofterms > longestcollartzvalue:
        print("Number of terms for", startValue, " is", numberofterms)
        longestcollartzvalue = numberofterms
        longestValue = startValue

print(longestValue)