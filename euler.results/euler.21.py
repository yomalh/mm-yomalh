# euler21
# amicable number

import math


def divisorssum(potential):
    list = []
    for i in range(2, int(math.sqrt(potential) + 1)):
        if potential % i == 0:
            i2 = potential / i
            list.append(i)
            list.append(i2)
    sum = 1
    for element in list:
        sum += element

    return sum


def divisors(pot):
    count = 0
    nextpot = divisorssum(pot)
    # print(nextpot)
    divsum = divisorssum(nextpot)
    # print(divsum)
    if divsum == pot:
        return pot, nextpot

    return 0, 0

# list with all pairs

amicables = []
#which are in 1-1000
for n in range(1, 10001):
    a, b = divisors(n)
    if a != b and a != 0 and b != 0:
        amicables.append(a)
        amicables.append(b)
sumamis =0
for i in amicables:
    sumamis += i
    print(i)
print("result: ",sumamis/2)
